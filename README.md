# btp-1-service-station (branch DEV)
SAP BTP Development: project 1. Information system for processing customer requests for equipment repair and warranty service. 

***Developed:*** partnership agreements module - information on the list of equipment serviced under warranty;
order service module - information about incoming orders, created service orders and acts of work performed (not implemented).

***Interaction:*** the client sends a request, the service engineer reviews the request and creates an order or rejects it. If the case is guaranteed, then it is confirmed by a technical expert and an accounting specialist. After that, the service order goes to work.

#### CPI
Customer requests come from an external system, such as a small mobile application. Next, integration processes come into play: the availability of the final system is checked, if yes, then an application is immediately formed in the database. If not, the information goes into the data store and then is processed again until a request is generated. If the system is available, but the request cannot be generated, then this information is placed in a new data store, the client is notified by e-mail (not implemented)

#### WF
Standard approval process similar to the tutorial.

#### CAP
A function has been implemented to request from the СPI stream information about the relevance of partner agreements, warranty periods by equipment serial number. Currently not in use.
